import anyjson
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

from video.models import Mapping, Video

@csrf_exempt
def policy(request):
    if request.method == 'POST':
        addresses = request.POST['addresses']
        addresses = anyjson.deserialize(addresses)
        colors = addresses.keys()
        mappings = Mapping.objects.filter(color__in=colors).order_by('priority')
        return render(request, 'video/policy.xml', {'mappings':mappings}, content_type='application/xml')

def list_videos(request):
    videos = Video.objects.all()
    return render(request, 'video/video_list.xml', {'videos':videos}, content_type='application/xml')
