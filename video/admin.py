from django.contrib import admin
from video.models import Mapping, Video

admin.site.register(Mapping)
admin.site.register(Video)
