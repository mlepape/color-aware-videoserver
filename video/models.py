from django.db import models

class Mapping(models.Model):
    
    pattern = models.CharField(max_length=255)
    color = models.IntegerField()
    priority = models.SmallIntegerField()

    def __unicode__(self):
        return u'Mapping (pattern: {0}, color: {1}, priority: {2})'.format(self.pattern, self.color, self.priority)

class Video(models.Model):

    name = models.CharField(max_length=255)
    file_basename = models.CharField(max_length=255)

    def __unicode__(self):
        return u'Video {0} contained in {1}.flv'.format(self.name, self.file_basename)

    def url(self):
        return u'http://video.maicolepape.org:8000/{0}.flv'.format(self.file_basename)

    def snapshot(self):
        return u'http://video.maicolepape.org/static/{0}.png'.format(self.file_basename)
